require('dotenv').config();
import homepageService from '../services/homepageService'
import chatbotService from '../services/chabotService';

const VERIFY_TOKEN = process.env.VERIFY_TOKEN;

let getHomePage = (req, res) => {
  return res.render('homepage.ejs')

};

let getWebHook = (req, res) => {
  // Your verify token. Should be a random string.
  // Parse the query params
  let mode = req.query['hub.mode'];
  let token = req.query['hub.verify_token'];
  let challenge = req.query['hub.challenge'];

  // Checks if a token and mode is in the query string of the request
  if (mode && token) {

    // Checks the mode and token sent is correct
    if (mode === 'subscribe' && token === VERIFY_TOKEN) {

      // Responds with the challenge token from the request
      console.log('WEBHOOK_VERIFIED');
      res.status(200).send(challenge);

    } else {
      // Responds with '403 Forbidden' if verify tokens do not match
      res.sendStatus(403);
    }
  }
};

let postWebHook = (req, res) => {
  let body = req.body;

  // Checks if this is an event from a page subscription
  if (body.object === 'page') {

    // Iterates over each entry - there may be multiple if batched
    body.entry.forEach(function (entry) {

      // Gets the body of the webhook event
      let webhookEvent = entry.messaging[0];
      console.log(webhookEvent);

      // Get the sender PSID
      let senderPsid = webhookEvent.sender.id;
      console.log('Sender PSID: ' + senderPsid);

      // Check if the event is a message or postback and
      // pass the event to the appropriate handler function
      if (webhookEvent.message) {
        handleMessage(senderPsid, webhookEvent.message);
      } else if (webhookEvent.postback) {
        handlePostback(senderPsid, webhookEvent.postback);
      }
    });

    // Returns a '200 OK' response to all requests
    res.status(200).send('EVENT_RECEIVED');
  } else {

    // Returns a '404 Not Found' if event is not from a page subscription
    res.sendStatus(404);
  }
};
// Handles messages events
let handleMessage = async (sender_psid, received_message) => {

  if (received_message && received_message.quick_reply && received_message.quick_reply.payload) {
    let payload = received_message.quick_reply.payload;
    console.log(payload);
    console.log(sender_psid);
    if (payload === 'CATEGORIAS') {
      console.log('SI');
      await chatbotService.sendCategorias(sender_psid);

    } /*else if (payload === "MOSTRAR_ORDEN") {
      await chatbotService.sendOrden(sender_psid);

    } else if (payload === "HABLAR_VENDEDOR") {
      await chatbotService.sendVendedor(sender_psid);

    }*/
    return;
  }

  let response;

  // Checks if the message contains text
  if (received_message.text) {
    // Create the payload for a basic text message, which
    // will be added to the body of our request to the Send API
    response = {
      "text": `You sent the message: "${received_message.text}". Now send me an attachment!`
    }
  } else if (received_message.attachments) {
    // Get the URL of the message attachment
    let attachment_url = received_message.attachments[0].payload.url;
    response = {
      "attachment": {
        "type": "template",
        "payload": {
          "template_type": "generic",
          "elements": [{
            "title": "Is this the right picture?",
            "subtitle": "Tap a button to answer.",
            "image_url": attachment_url,
            "buttons": [
              {
                "type": "postback",
                "title": "Yes!",
                "payload": "yes",
              },
              {
                "type": "postback",
                "title": "No!",
                "payload": "no",
              }
            ],
          }]
        }
      }
    }
  }

  // Send the response message
  //callSendAPI(sender_psid, response);
  await chatbotService.sendMessage(sender_psid, response);

}

// Handles messaging_postbacks events
let handlePostback = async (sender_psid, received_postback) => {
  let response;

  // Get the payload for the postback
  let payload = received_postback.payload;
  // Set the response based on the postback payload
  switch (payload) {
    case "yes":
      response = { "text": "Thanks!" };
      break;
    case "no":
      response = { "text": "Oops, try sending another image." }
      break;
    case "GET_STARTED":
      await chatbotService.sendMessageWelcomeUser(sender_psid)
      break;
    default:
      console.log('Default');
  }

};


let handleSetupProfile = async (req, res) => {
  try {
    await homepageService.handleSetupProfileAPI();
    return res.redirect('/');
  } catch (e) {
    console.log(e);
  }

}

let getSetupProfielaPage = (req, res) => {
  return res.render('profile.ejs');
}

module.exports = {
  getHomePage: getHomePage,
  getWebHook: getWebHook,
  postWebHook: postWebHook,
  handleSetupProfile,
  getSetupProfielaPage,
}