import  express  from "express";
import homepageController from "../controllers/homepageController";

let router= express.Router();

let initialWebRoutes=(app)=>{
    router.get('/',homepageController.getHomePage);
    router.get('/webhook',homepageController.getWebHook);
    router.post('/webhook',homepageController.postWebHook);
    router.post('/set-up-profile',homepageController.handleSetupProfile);
    router.get('/set-up-profile',homepageController.getSetupProfielaPage);


    return app.use('/',router);
}
module.exports=initialWebRoutes;