require('dotenv').config();
import request from 'request';
import homepageService from './homepageService';
const PAGE_ACCESS_TOKEN = process.env.PAGE_ACCESS_TOKEN;

let sendMessage = (sender_psid, response) => {
    
    return new Promise(async (resolve, reject) => {
        try {
            // Construct the message body
            await homepageService.markMessageRead(sender_psid);
            await homepageService.sendTypingOn(sender_psid);
            let request_body = {
                "recipient": {
                    "id": sender_psid
                },
                "message": response
            };

            // Send the HTTP request to the Messenger Platform
            request({
                "uri": "https://graph.facebook.com/v6.0/me/messages",
                "qs": { "access_token": PAGE_ACCESS_TOKEN },
                "method": "POST",
                "json": request_body
            }, (err, res, body) => {
                if (!err) {
                    resolve('message sent!')
                    console.log('message sent!')
                } else {
                    reject("Unable to send message:" + err);
                }
            });

        } catch (e) {
            reject(e);
        }
    });
}

let sendMessageWelcomeUser = (sender_psid) => {
    return new Promise(async (resolve, reject) => {
        try {
            let username = await homepageService.getFacebookUsername(sender_psid);

            //send text
            let response1 = {
                "text": `Hola ${username}, Bienvenido a la Pagina`
            };

            //send an image
            let response2 = {
                "attachment": {
                    "type": "image",
                    "payload": {
                        "url": "https://image.freepik.com/vector-gratis/logo-degradado-forma-abstracta_23-2148217261.jpg",
                    }
                }
            };

            let response3 = {
                "text": `Aqui  ${username}, Econtraras de programacion`
            };

            //send a quick reply
            let response4 = {
                "text": "en que puedo ayudarte?",
                "quick_replies": [
                    {
                        "content_type": "text",
                        "title": "Categorias",
                        "payload": "CATEGORIAS",
                        // "image_url":"http://example.com/img/red.png"
                    }, 
                    {
                        "content_type": "text",
                        "title": "Mostrar Orden",
                        "payload": "MOSTRAR_ORDEN",
                    }
                    , 
                    {
                        "content_type": "text",
                        "title": "Hablar con vendedor",
                        "payload": "HABLAR_VENDEDOR",

                    }
                ]

            };
            await sendMessage(sender_psid, response1);
            await sendMessage(sender_psid, response2);
            await sendMessage(sender_psid, response3);
            await sendMessage(sender_psid, response4);
            resolve('Done');
        } catch (e) {
            reject(e);
        }
    });
}

let sendCategorias = (sender_psid) => {
    console.log('adentro de categorias',sender_psid)
    return new Promise(async (resolve, reject) => {
        try {
            let response = {
                "attachment":{
                    "type":"template",
                    "payload":{
                      "template_type":"generic",
                      "elements":[
                         {
                          "title":"Welcome!",
                          "image_url":"https://image.shutterstock.com/image-illustration/minimal-background-branding-packaging-presentation-260nw-1567863046.jpg",
                          "subtitle":"We have the right hat for everyone.",
                          "default_action": {
                            "type": "web_url",
                            "url": "https://petersfancybrownhats.com/view?item=103",
                            "webview_height_ratio": "tall",
                          },
                          "buttons":[
                            {
                              "type":"web_url",
                              "url":"https://petersfancybrownhats.com",
                              "title":"View Website"
                            },{
                              "type":"postback",
                              "title":"Start Chatting",
                              "payload":"DEVELOPER_DEFINED_PAYLOAD"
                            }              
                          ]      
                        },
                        {
                            "title":"Welcome!",
                            "image_url":"https://image.shutterstock.com/image-illustration/minimal-background-branding-packaging-presentation-260nw-1567863046.jpg",
                            "subtitle":"We have the right hat for everyone.",
                            "default_action": {
                              "type": "web_url",
                              "url": "https://petersfancybrownhats.com/view?item=103",
                              "webview_height_ratio": "tall",
                            },
                            "buttons":[
                              {
                                "type":"web_url",
                                "url":"https://petersfancybrownhats.com",
                                "title":"View Website"
                              },{
                                "type":"postback",
                                "title":"Start Chatting",
                                "payload":"DEVELOPER_DEFINED_PAYLOAD"
                              }              
                            ]      
                          },
                          {
                            "title":"Welcome!",
                            "image_url":"https://image.shutterstock.com/image-illustration/minimal-background-branding-packaging-presentation-260nw-1567863046.jpg",
                            "subtitle":"We have the right hat for everyone.",
                            "default_action": {
                                "type": "web_url",
                                "url": "https://petersfancybrownhats.com/view?item=103",
                                "webview_height_ratio": "tall",
                              },
                            "buttons":[
                              {
                                "type":"web_url",
                                "url":"https://petersfancybrownhats.com",
                                "title":"View Website"
                              },{
                                "type":"postback",
                                "title":"Start Chatting",
                                "payload":"DEVELOPER_DEFINED_PAYLOAD"
                              }              
                            ]      
                          }

                      ]
                    }
                  }

            };
            await sendMessage(sender_psid, response);
            console.log('si envia cate');
            resolve('Done');
        } catch (e) {
            reject(e);

        }
    });
};

let sendOrden = (sender_psid) => {
    return new Promise((resolve, reject) => {
        try {

        } catch (e) {
            reject(e);

        }
    });
}

let sendVendedor = (sender_psid) => {
    return new Promise((resolve, reject) => {
        try {

        } catch (e) {
            reject(e);

        }
    });
}
module.exports = {
    sendMessage,
    sendMessageWelcomeUser,
    sendCategorias,
    sendOrden,
    sendVendedor
}